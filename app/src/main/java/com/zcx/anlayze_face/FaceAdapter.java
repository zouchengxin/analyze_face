package com.zcx.anlayze_face;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.megvii.facepp.api.bean.CommonRect;
import com.megvii.facepp.api.bean.DetectResponse;
import com.megvii.facepp.api.bean.Face;
import com.megvii.facepp.api.bean.FaceAttributes;

import java.util.List;

public class FaceAdapter extends RecyclerView.Adapter<ViewHolder> {
    private List<Face> list;
    private Context context;

    public FaceAdapter(Context context){
        this.context=context;
        DetectResponse detectResponse = ImageResource.getInstance().getDetectResponse();
        if(detectResponse!=null){
            this.list=detectResponse.getFaces();
        }

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.face_item, viewGroup,false);
        ViewHolder viewHolder = new ViewHolder(view) {
            @Override
            public String toString() {
                return super.toString();

            }
        };
        return  viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        View view = viewHolder.itemView;
        FaceAttributes attributes = list.get(i).getAttributes();
        if(attributes==null){
            Toast.makeText(context,"加载失败!",Toast.LENGTH_LONG).show();
            return;
        }
        //System.out.println("item"+jsonObject.toJSONString());
        setItemListener(view,list.get(i).getFace_token());
        ImageView iv = view.findViewById(R.id.iv);
        TextView tv_age = view.findViewById(R.id.age);
        TextView tv_beauty = view.findViewById(R.id.beauty);
        TextView tv_gender = view.findViewById(R.id.gender);
        String age = attributes.getAge().getValue();
        String gender = attributes.getGender().getValue();
        float beauty=0;
        if(gender.equals("Female")){
            gender="女性";
            beauty= attributes.getBeauty().getFemale_score();
        }else {
            gender="男性";
            beauty= attributes.getBeauty().getFemale_score();
        }

        tv_age.setText(tv_age.getText()+age);
        tv_gender.setText(tv_gender.getText()+gender);
        tv_beauty.setText(tv_beauty.getText()+String.valueOf(beauty));

        CommonRect face_rect = list.get(i).getFace_rectangle();
        Bitmap bitmap = Bitmap.createBitmap(
                ImageResource.getInstance().getOrig_img()
                ,face_rect.getLeft()
                ,face_rect.getTop()
                ,face_rect.getWidth()
                ,face_rect.getHeight()
        );
        iv.setImageBitmap(bitmap);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private void setItemListener(View view, final String face_token){
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(context,DetailActivity.class);
                intent.putExtra("face_token",face_token);
                context.startActivity(intent);
            }
        });
    }

}
