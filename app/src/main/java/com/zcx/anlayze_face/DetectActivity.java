package com.zcx.anlayze_face;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class DetectActivity extends AppCompatActivity {
    private RecyclerView rv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("人脸分析");
        setContentView(R.layout.detect_face);

        rv=findViewById(R.id.rv);
        Intent intent = getIntent();
        fillData();
    }

    //填充数据到rv
    private void fillData(){
        rv.setAdapter(new FaceAdapter(this));
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.HORIZONTAL));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        rv=null;
    }
}
