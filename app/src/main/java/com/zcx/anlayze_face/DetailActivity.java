package com.zcx.anlayze_face;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.megvii.facepp.api.bean.DetectResponse;
import com.megvii.facepp.api.bean.Face;
import com.megvii.facepp.api.bean.FaceAttributes;

import java.util.HashMap;
import java.util.Map;

public class DetailActivity extends AppCompatActivity {
    private FaceAttributes attributes;
    private Map<String, TextView> map;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("详情");
        setContentView(R.layout.detail_activity);
        Intent intent = getIntent();
        String face_token = intent.getStringExtra("face_token");
        DetectResponse response = ImageResource.getInstance().getDetectResponse();
        if(response!=null){
            for (Face face:response.getFaces()){
                if(face.getFace_token().equals(face_token)){
                    attributes=face.getAttributes();
                }
            }
        }
        map=new HashMap<>();
        map.put("tv_gender",(TextView)findViewById(R.id.gender));
        map.put("tv_age",(TextView)findViewById(R.id.age));
        map.put("tv_smile",(TextView)findViewById(R.id.smile));
        map.put("tv_glass",(TextView)findViewById(R.id.glass));
        map.put("tv_blur",(TextView)findViewById(R.id.blur));
        map.put("tv_emotion",(TextView)findViewById(R.id.emotion));
        map.put("tv_facequality",(TextView)findViewById(R.id.facequality));
        map.put("tv_ethnicity",(TextView)findViewById(R.id.ethnicity));
        map.put("tv_beauty",(TextView)findViewById(R.id.beauty));
        map.put("tv_skinstatus",(TextView)findViewById(R.id.skinstatus));
        map.put("tv_headpose",(TextView)findViewById(R.id.headpose));
        setValue();
    }

    //设置TextView的值
    private void setValue(){
        if(attributes==null){
            Toast.makeText(this,"没有数据",Toast.LENGTH_LONG).show();
            return;
        }
        if(attributes.getGender()!=null) {
            map.get("tv_gender").append(attributes.getGender().getValue());
        }
        if(attributes.getAge()!=null){
            map.get("tv_age").append(attributes.getAge().getValue());
        }
        if(attributes.getSmiling()!=null) {
            map.get("tv_smile").append(attributes.getSmiling().getValue());
        }
        if(attributes.getGlass()!=null) {
            String value = attributes.getGlass().getValue();
            if(value.equals("None")){
                map.get("tv_glass").append("不佩戴眼镜");
            }
            if(value.equals("Dark")){
                map.get("tv_glass").append("佩戴墨镜");
            }
            if(value.equals("Normal")){
                map.get("tv_glass").append("佩戴普通眼镜");
            }
        }
        if(attributes.getBlur()!=null) {
            String blurn_value = attributes.getBlur().getBlurness().getValue();
            String gauss_value = attributes.getBlur().getGaussianblur().getValue();
            String motion_value = attributes.getBlur().getMotionblur().getValue();
            map.get("tv_blur").append("Blurn:"+String.valueOf(blurn_value)+"\n"
                    +"Gaussian:"+ String.valueOf(gauss_value)+"\n"
                    +"Motion:"+String.valueOf(motion_value));
        }
        if(attributes.getEmotion()!=null) {
            float anger = attributes.getEmotion().getAnger();
            float disgust = attributes.getEmotion().getDisgust();
            float fear = attributes.getEmotion().getFear();
            float happiness = attributes.getEmotion().getHappiness();
            float neutral = attributes.getEmotion().getNeutral();
            float sadness = attributes.getEmotion().getSadness();
            float surprise = attributes.getEmotion().getSurprise();
            map.get("tv_emotion").append("anger(愤怒):"+String.valueOf(anger)+"\n"
                    +"anger(愤怒):"+String.valueOf(anger)+"\n"
                    +"disgust(厌恶):"+String.valueOf(disgust)+"\n"
                    +"fear(恐惧):"+String.valueOf(fear)+"\n"
                    +"happines(高兴):"+String.valueOf(happiness)+"\n"
                    +"neutra(平静):"+String.valueOf(neutral)+"\n"
                    +"sadness(伤心):"+String.valueOf(sadness)+"\n"
                    +"surprise(惊讶):"+String.valueOf(surprise)
                    );
        }
        if(attributes.getFacequality()!=null) {
            map.get("tv_facequality").append(attributes.getFacequality().getValue());
        }
        if(attributes.getEthnicity()!=null) {
            String value = attributes.getEthnicity().getValue();
            if(value.equals("Asian")){
                map.get("tv_ethnicity").append("亚洲人");
            }
            if(value.equals("White")){
                map.get("tv_ethnicity").append("白人");
            }
            if(value.equals("Black")){
                map.get("tv_ethnicity").append("黑人");
            }

        }
        if(attributes.getBeauty()!=null) {
            float female_score = attributes.getBeauty().getFemale_score();
            float male_score = attributes.getBeauty().getMale_score();
            map.get("tv_beauty").append("男性认为的此人脸颜值分数:"+male_score+"\n"
                    +"女性认为的此人脸颜值分数:"+female_score);
        }
        if(attributes.getSkinstatus()!=null) {
            float health = attributes.getSkinstatus().getHealth();
            float acne = attributes.getSkinstatus().getAcne();
            float dark_circle = attributes.getSkinstatus().getDark_circle();
            float stain = attributes.getSkinstatus().getStain();
            map.get("tv_skinstatus").append("healt(健康):"+health+"\n"
                    +"stain(色斑):"+stain+"\n"
                    +"acn(青春痘):"+acne+"\n"
                    +"dark_circle(黑眼圈):"+dark_circle);
        }
        if(attributes.getHeadpose()!=null){
            float pitch_angle = attributes.getHeadpose().getPitch_angle();
            float roll_angle = attributes.getHeadpose().getRoll_angle();
            float yaw_angle = attributes.getHeadpose().getYaw_angle();
            map.get("tv_headpose").append("pitch_angl(抬头):"+pitch_angle+"\n"
                    +"roll_angle(平面旋转):"+roll_angle+"\n"
                    +"yaw_angle(摇头):"+yaw_angle);
        }
    }

}
