package com.zcx.anlayze_face;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;
import com.megvii.facepp.api.FacePPApi;
import com.megvii.facepp.api.IFacePPCallBack;
import com.megvii.facepp.api.bean.BeautyResponse;
import com.megvii.facepp.api.bean.DetectResponse;

import java.util.HashMap;
import java.util.Map;

public class FaceUtils {
    private String TAG="FaceUtils";
    private String api_key="_XkXB2jly_0ZteVlGNxI8cT6S8XE4Gnj";
    private String api_secret="guR94t2Pwg_0DKNTyt3QPe7iAQBuCtpa";
    private FacePPApi faceApi;

    public FaceUtils(){
        faceApi=new FacePPApi(api_key,api_secret);
    }

    //传入图片进行人脸检测和人脸分析
    public void detect_face(final AppCompatActivity activity, byte[] img){
        Map<String,String> map=new HashMap<>();
        map.put("return_landmark","1");
        map.put("return_attributes","gender,age,smiling,headpose,facequality,blur,eyestatus,emotion,ethnicity,beauty,mouthstatus,eyegaze,skinstatus");
        faceApi.detect(map,img,new IFacePPCallBack<DetectResponse>() {
            @Override
            public void onSuccess(DetectResponse detectResponse) {
                String error = detectResponse.getError_message();
                if(error!=null){
                    Toast.makeText(activity,error,Toast.LENGTH_LONG).show();
                    Log.e(TAG,error);
                    return;
                }
                System.out.println("detect:"+detectResponse.toString());
                Intent intent=new Intent();
                intent.setClass(activity,DetectActivity.class);
                ImageResource.getInstance().setDetectResponse(detectResponse);
                //intent.putExtra("data", JSONObject.toJSONString(detectResponse));
                activity.startActivityForResult(intent,2);
            }

            @Override
            public void onFailed(String s) {
                Log.e(TAG, s);
            }
        });
    }

    //对图片进行美颜和美白
    public void beauty_face(final AppCompatActivity activity, byte[] img){
        HashMap<String, String> map = new HashMap<>();
        faceApi.beautify(map, img, new IFacePPCallBack<BeautyResponse>() {
            @Override
            public void onSuccess(BeautyResponse beautyResponse) {
                String error = beautyResponse.getError_message();
                if(error!=null){
                    Toast.makeText(activity,error,Toast.LENGTH_LONG).show();
                    Log.e(TAG,error);
                    return;
                }
                String result = beautyResponse.getResult();
                byte[] decode = Base64.decode(result, Base64.DEFAULT);
                ImageResource.getInstance().setBeauty_img(BitmapFactory.decodeByteArray(decode,0,decode.length));
                Intent intent = new Intent();
                intent.setClass(activity,BeautyActivity.class);
                activity.startActivityForResult(intent,3);
            }

            @Override
            public void onFailed(String s) {
                Log.e(TAG, s);
            }
        });
    }

}
